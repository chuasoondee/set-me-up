#!/usr/bin/env bash
set -euxo pipefail

DIR="$(cd "$(dirname "${BASE_SOURCE[0]}")" && pwd)"
PLATFORM="$(uname -o)"
SHELLRC=""
INSTALL=""

declare -a platforms=("Cygwin" "Msys" "GNU/Linux" "Darwin") # not in use
declare -a pkg=("pact" "pacman" "apt-cyg")
declare -a pkgopt=("install" "-S --noconfirm --needed" "install")

function checkout() {
    src=$1
    dest=$2
    if [ ! -d $dest ]; then
        git clone $src $dest
    fi
}

case "$PLATFORM" in
	"Cygwin" | "Msys" )
		SHELLRC=".bashrc"

		for j in "${!pkg[@]}"; do
			command -v "${pkg[j]}" || continue
			INSTALL="$(command -v ${pkg[j]}) ${pkgopt[j]}"
			break
		done

		# default install apt-cyg if none exists
		[ "${INSTALL}x" == "x" ] && { \
			install "$DIR/resources/bin/apt-cyg" /bin; \
			INSTALL="/bin/apt-cyg install"
		}

		break
		;;

	"GNU/Linux" )
		SHELLRC=".bashrc"
		INSTALL="$(command -v apt) install -y"
		break
		;;

	"Darwin" )
		SHELLRC=".bash_profile"
		[ $(command -v brew) ] || \
			ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
		INSTALL="$(command -v brew) install"
		break
		;;

	* )
		echo "Setup.sh ony works on Cygwin, Msys, Babun, Debian and Darwin distribution only"
		exit 1
		;;
esac

[ "${INSTALL}x" == "x" ] && { \
    echo "This scripts run on cygwin, msys64, wsl, debian & darwin distribution only"; \
    exit 1; \
}

#
# Install standard tools
#
TOOLS=("vim" "git" "curl" "tmux" "unzip" "tar")
for d in "${TOOLS[@]}"; do
	sudo $INSTALL $d
done

#
# dotfiles
#
for i in "${DIR}/resources/dotfiles"/*; do
    cp -r "$i" "$HOME/.$(basename $i)"
done

#
# tmux theme
#
grep 'source-file "${HOME}/.tmux-themepack' "$HOME/.tmux.conf" || exitcode=$?
if (( exitcode != 0 )); then
    checkout 'https://github.com/jimeh/tmux-themepack.git' "$HOME/.tmux-themepack"
    echo 'source-file "${HOME}/.tmux-themepack/powerline/block/blue.tmuxtheme"' >> "$HOME/.tmux.conf"
fi

#
# ps1
#
grep 'source $HOME/.pureline/pureline' "$HOME/$SHELLRC" || exitcode=$?
if (( exitcode != 0 )); then
    checkout 'https://github.com/chris-marsh/pureline.git' "$HOME/.pureline"
	echo 'PL_USER_SHOW_HOST=false' >> "$HOME/.bashrc"
    echo 'source "${HOME}/.pureline/pureline" "${HOME}/.pureline/configs/powerline_full_256col.conf"' >> "$HOME/.bashrc"
fi

#
# alias
#
grep 'source $HOME/.alias' "$HOME/$SHELLRC" || exitcode=$?
if (( exitcode != 0 )); then
    echo 'source "$HOME/.alias"' >> "$HOME/$SHELLRC"
fi

#
# dircolors
#
grep '.dircolors/solarized' "$HOME/$SHELLRC" || exitcode=$?
if (( exitcode != 0 )); then
    echo '[ -f "$HOME/.dircolors/solarized" ] && eval $(dircolors -b "$HOME/.dircolors/solarized")' >> "$HOME/$SHELLRC"
fi

#
# Install vundle and plugins
#
if [ ! -d "$HOME/.vim/bundle/Vundle.vim" ]; then
    checkout 'https://github.com/VundleVim/Vundle.vim.git' "$HOME/.vim/bundle/Vundle.vim"
    vim +PluginInstall +qall
fi
