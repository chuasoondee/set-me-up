#!/bin/bash

[ $(command -v brew) ] || ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

[ $(command -v ansible) ] || brew install ansible
