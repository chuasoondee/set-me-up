# Set Me Up
To automate my common toolsets (with customization) 
on MacOSX, Window Cygwin/Msys2 and Debian.

## Usage
Launch terminal and run `cd set-me-up && ./setup.sh`.

Do remember to manually install the included fonts. For Windows, install the
fonts with "Window Compatible" file name.

## Software Installed
1. Vim
2. Vim Plugins
    * [Vundle](https://github.com/VundleVim/Vundle.vim)
    * [vim-airline](https://github.com/vim-airline/vim-airline-themes)
    * [vim-airline-themes](https://github.com/vim-airline/vim-airline)
    * [vim-devicons](https://github.com/ryanoasis/vim-devicons)
    * [vim-go](https://github.com/fatih/vim-go)
    * [vim-json](https://github.com/elzr/vim-json)
    * [onedark.vim](https://github.com/joshdick/onedark.vim)
    * [vim-polyglot](https://github.com/sheerun/vim-polyglot)
3. Git
4. Curl
5. Tmux
6. Unzip
7. Tar

## Dotfiles Customization
1. Vimrc
2. Tmux config and Theme
3. Minttyrc (Only on Windows)
4. Bash Alias
5. Dircolors

## Fonts
Fonts will have to be manually copy/install.

## IMPORTANT
Below method uses ansible and only supports MacOSX. `setup.sh` will work on 
MacOSX less the application installation via Brew Cask. These will be backport 
to `setup.sh` in the future but for now `setup.sh` and ansible has different 
resources and execution thus it is recommended to use `setup.sh` for now.

### To Run
1. `./bootstrap.sh` Install homebrew and ansible.
2. `ansible-playbook -i inventory playbook.yml` to start automation

## List of Softwares installed.
(See playbook.yml for updated)

1. google-chrome
2. iterm2
3. mplayer-osx-extended
4. transmission
5. caffeine
6. spectacle
7. virtualbox
8. vagrant

